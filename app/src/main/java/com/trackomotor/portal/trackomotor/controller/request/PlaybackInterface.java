package com.trackomotor.portal.trackomotor.controller.request;

import com.google.gson.JsonElement;
import com.trackomotor.portal.trackomotor.model.PositionRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by bishal21 on 2/26/2018.
 */

public interface PlaybackInterface {
    @Headers({
            "Content-Type: application/json"
    })

    @GET
    Call<JsonElement> getPositions(
            @Header("api-key") String apiKey,
            @Header("access-token") String accessToken,
            @Url String url
    );

    @POST("device/play-back")
    Call<JsonElement> getPosition(
            @Header("api-key") String apiKey,
            @Header("access-token") String accessToken,
            @Body PositionRequest positionRequest
            );
}
