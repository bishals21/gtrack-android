package com.trackomotor.portal.trackomotor.model

/**
 * Created by bishal21 on 2/25/2018.
 */



class DepartmentsVechicle(var department: String?,var number: String?,var code: String?,var deviceId: Int?,var deviceUniqueId: String?, var deviceName: String?,
                          var deviceStatus: String?,var vehicleType: String?,var vehicleTypeImage: String?
                          ,var overSpeedLimit: Int?,var lastUpdate: String?,var longitude: String?,
                          var latitude: String?,var address: String?,var speed: String?,var ignition: String?,var charge: String?,var battery: String?
                          ,var rssi: String?,var course: String?,var alarm: String?,var lastPositionTime: String?,
                          var event: String?,var lastEventTime: String?) {

    override fun toString(): String {
        return deviceName!!
    }
}
