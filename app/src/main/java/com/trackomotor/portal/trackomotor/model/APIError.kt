package com.trackomotor.portal.trackomotor.model

/**
 * Created by bishal21 on 1/17/2018.
 */

class APIError {

    private val status: Boolean = false
    private val message: String? = null

    fun status(): Boolean {
        return status
    }

    fun message(): String? {
        return message
    }
}

