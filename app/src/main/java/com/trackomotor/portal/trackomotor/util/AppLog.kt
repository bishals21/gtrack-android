package com.trackomotor.portal.trackomotor.util


import android.util.Log

import com.trackomotor.portal.trackomotor.BuildConfig


/**
 * Created by Bishal on 4/16/15.
 */
object AppLog {

    fun showLog(tag: String, message: String) {
        //  if(BuildConfig.DEBUG) Log.i(tag, message);
    }

    fun i(`object`: Any, message: String): Int {
        return if (BuildConfig.DEBUG) {
            Log.i(`object`.javaClass.simpleName, message)
        } else 0
    }

    fun v(`object`: Any, message: String): Int {
        return if (BuildConfig.DEBUG) {
            Log.v(`object`.javaClass.simpleName, message)
        } else 0
    }

    fun e(`object`: Any, message: String): Int {
        return if (BuildConfig.DEBUG) {
            Log.e(`object`.javaClass.simpleName, message)
        } else 0
    }

}
