package com.trackomotor.portal.trackomotor.activity

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolygonOptions
import com.trackomotor.portal.trackomotor.R


class GeofenceDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    var area: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geofence_detail)

        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (intent.extras != null) {
            val title = intent.getStringExtra("title")
            actionBar.title = title
            area = intent.getStringExtra("area")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (area != null) {
            val areaBeforeBracket = area!!.substringBefore("(")
            if (areaBeforeBracket.trim() == "CIRCLE") {
                val values = area!!.substring(area!!.indexOf("(") + 1, area!!.indexOf(")"))
                Log.e("Circle", values)
                val splitValues = values.split(", ")
                val latLng = splitValues[0]
                val radius = splitValues[1].toDouble()

                val splitLatLng = latLng.split(" ")
                val lat: Double = splitLatLng[0].toDouble()
                val lng = splitLatLng[1].toDouble()
                val circleOptions = CircleOptions()
                        .center(LatLng(lat, lng))
                        .radius(radius).strokeColor(Color.BLACK)
                        .strokeWidth(2f).fillColor(0x500000ff)
                mMap.addCircle(circleOptions)
                val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(lat, lng))
                        .zoom(13f)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            } else if (areaBeforeBracket.trim() == "POLYGON") {
                val lngLat = ArrayList<LatLng>()
                val values = area!!.substring(area!!.indexOf("(") + 2, area!!.indexOf(")"))
                Log.e("Polygon", values)
                val splitValues = values.split(",")
                for (i in 0 until splitValues.size) {
                    val insideValue = splitValues[i].split(" ")
                    val lat = insideValue[0].toDouble()
                    val lng = insideValue[1].toDouble()
                    lngLat.add(LatLng(lat, lng))
                }
                val polygonOptions = PolygonOptions()
                        .addAll(lngLat)
                        .strokeColor(Color.BLACK)
                        .strokeWidth(2f).fillColor(0x500000ff)
                mMap.addPolygon(polygonOptions)
                val cameraPosition = CameraPosition.Builder()
                        .target(lngLat[0])
                        .zoom(13f)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }
        }
    }
}
