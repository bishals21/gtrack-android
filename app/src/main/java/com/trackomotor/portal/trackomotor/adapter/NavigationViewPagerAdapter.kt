package com.trackomotor.portal.trackomotor.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.trackomotor.portal.trackomotor.fragment.MenuFragment
import com.trackomotor.portal.trackomotor.fragment.UserInfoFragment
import com.trackomotor.portal.trackomotor.fragment.DeviceMessageFragment

/**
 * Created by bishal21 on 3/7/2018.
 */
class NavigationViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return UserInfoFragment()
            1 -> return MenuFragment()
        }

        return null
    }

    /*override fun getPageTitle(position: Int): CharSequence {
        return tabs[position]
    }*/

}