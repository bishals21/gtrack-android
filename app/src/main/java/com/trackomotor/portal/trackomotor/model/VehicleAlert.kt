package com.trackomotor.portal.trackomotor.model

/**
 * Created by bishal21 on 2/25/2018.
 */

class VehicleAlert(var location: String?, var latitude: String?, var longitude: String?, var time: String?, var vehicle: String?, var type: String?)
