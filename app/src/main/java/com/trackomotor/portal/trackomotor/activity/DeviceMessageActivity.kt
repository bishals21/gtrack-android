package com.trackomotor.portal.trackomotor.activity

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.squareup.otto.Subscribe
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.adapter.DeviceMessageAdapter
import com.trackomotor.portal.trackomotor.controller.request.AlertTask
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.model.VehicleAlert
import com.trackomotor.portal.trackomotor.util.AppText
import com.trackomotor.portal.trackomotor.util.AppUtil
import com.trackomotor.portal.trackomotor.util.DividerItemDecoration
import kotlinx.android.synthetic.main.activity_device_message.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class DeviceMessageActivity : AppCompatActivity(), DeviceMessageAdapter.OnLoadMoreListener {


    private var vehicleAlertList: MutableList<VehicleAlert>? = null
    private var nextVehicleAlertList: MutableList<VehicleAlert>? = null

    private var deviceMessageAdapter: DeviceMessageAdapter? = null

    private var dataObject: JSONObject? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_message)
        EventBus.register(this)

        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar.title = "Device Message"
        /*actionBar.setDefaultDisplayHomeAsUpEnabled(true)*/

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.setLayoutManager(layoutManager)
        recyclerView.setHasFixedSize(true)
        recyclerView.setItemAnimator(DefaultItemAnimator())
        recyclerView.addItemDecoration(DividerItemDecoration(this, R.drawable.divider))
        deviceMessageAdapter = DeviceMessageAdapter(this)
        deviceMessageAdapter!!.setLinearLayoutManager(layoutManager)
        deviceMessageAdapter!!.setRecyclerView(recyclerView)
        recyclerView.setAdapter(deviceMessageAdapter)
        swipeRefreshLayout.setOnRefreshListener { fetchData() }
        fetchData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun fetchData() {
        if (AppUtil.isNetworkConnected(this)) {
            startRefresing()
            AlertTask().alertRequest(AppText.API_KEY, UserAccessToken.userToken.token)
        } else {
            stopRefresing()
            Toast.makeText(this, "No Internet Connection!!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun startRefresing() {
        swipeRefreshLayout.post { swipeRefreshLayout.isRefreshing = true }
    }

    private fun stopRefresing() {
        swipeRefreshLayout.isRefreshing = false
        val handler = Handler()
        handler.postDelayed({ swipeRefreshLayout.isRefreshing = false }, 100)
    }

    @Subscribe
    fun VehicleAlert(event: Events.VehicleAlert?) {
        stopRefresing()
        if (event != null) {
            val jsonObject = event.alertResponse
            vehicleAlertList = ArrayList<VehicleAlert>()
            try {
                if (jsonObject.getBoolean("status")) {
                    dataObject = jsonObject.getJSONObject("data")
                    val alertObject: JSONArray = dataObject!!.getJSONArray("alerts")
                    for (i in 0 until alertObject.length()) {
                        val item = alertObject.get(i) as JSONObject

                        val location = item.getString("location")
                        val latitude = item.getString("latitude")
                        val longitude = item.getString("longitude")
                        val time = item.getString("time")
                        val vehicle = item.getString("vehicle")
                        val type = item.getString("type")
                        vehicleAlertList!!.add(VehicleAlert(location, latitude, longitude, time, vehicle, type))
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            Log.e("Vehicle List", vehicleAlertList.toString())
            deviceMessageAdapter!!.addAll(vehicleAlertList)
        }

    }

    @Subscribe
    fun NextVehicleAlert(event: Events.NextVehicleAlert?) {
        deviceMessageAdapter!!.setProgressMore(false)
        if (event != null) {
            val jsonObject = event.alertResponse
            Log.e("Json", jsonObject.toString())
            nextVehicleAlertList = ArrayList<VehicleAlert>()
            try {
                if (jsonObject.getBoolean("status")) {
                    dataObject = jsonObject.getJSONObject("data")
                    val alertObject: JSONArray = dataObject!!.getJSONArray("alerts")
                    for (i in 0 until alertObject.length()) {
                        val item = alertObject.get(i) as JSONObject

                        val location = item.getString("location")
                        val latitude = item.getString("latitude")
                        val longitude = item.getString("longitude")
                        val time = item.getString("time")
                        val vehicle = item.getString("vehicle")
                        val type = item.getString("type")
                        nextVehicleAlertList!!.add(VehicleAlert(location, latitude, longitude, time, vehicle, type))
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            Log.e("Next Vehicle List", nextVehicleAlertList.toString())
            Handler().postDelayed({
                vehicleAlertList!!.clear()
                for (i in nextVehicleAlertList!!.indices) {
                    val rechargeCard = nextVehicleAlertList!!.get(i)
                    vehicleAlertList!!.add(rechargeCard)
                }
                deviceMessageAdapter!!.addItemMore(vehicleAlertList)
                deviceMessageAdapter!!.setMoreLoading(false)
            }, 1000)
        }

    }

    override fun onLoadMore() {
        if (AppUtil.isNetworkConnected(this)) {
            Log.i("network", "connected")
            if (dataObject!!.getInt("nextPage") > 0) {
                val currentPage: Int = dataObject!!.getInt("currentPage") + 1
                Log.e("Current page", currentPage.toString())
                val url: String = "http://portal.trackomotor.com/api/v1/company/event-alerts?page=" + currentPage
                deviceMessageAdapter!!.setProgressMore(true)
                try {
                    AlertTask().alertRequestNext(AppText.API_KEY, UserAccessToken.userToken.token, url)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
        } else {
            Log.i("network", "not connected")
            Snackbar.make(constraintLayout, "Internet is not connected", Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.unregister(this)
    }

}
