package com.trackomotor.portal.trackomotor.adapter;
import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.trackomotor.portal.trackomotor.R;
import com.trackomotor.portal.trackomotor.model.InfoWindowData;


public class InfoAdapter implements InfoWindowAdapter {
    private View popup=null;
    private LayoutInflater inflater=null;

    public  InfoAdapter(LayoutInflater inflater) {
        this.inflater=inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return(null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        if (popup == null) {
            popup=inflater.inflate(R.layout.info_window, null);
        }

        TextView timeTv=(TextView)popup.findViewById(R.id.marker_time);
        TextView latLngTv=(TextView)popup.findViewById(R.id.marker_lat_lng);
        TextView addressTv=(TextView)popup.findViewById(R.id.marker_address);
        TextView speedTv=(TextView)popup.findViewById(R.id.marker_speed);

        InfoWindowData info = (InfoWindowData) marker.getTag();
        timeTv.setText("Time: ");
        latLngTv.setText("Lat: "+info.getLat()+", Lng: "+info.getLng());
        addressTv.setText("Address: "+info.getAddress());
        speedTv.setText("Speed: "+info.getSpeed()+" km/hr");
        return(popup);
    }
}
