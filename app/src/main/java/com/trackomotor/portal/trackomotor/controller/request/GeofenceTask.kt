package com.trackomotor.portal.trackomotor.controller.request

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.controller.RetrofitAdapter
import com.trackomotor.portal.trackomotor.util.AppLog
import com.trackomotor.portal.trackomotor.util.ErrorUtils
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GeofenceTask {
    fun geofenceRequest(apiKey: String, accessToken: String) {
        val retrofit = RetrofitAdapter().adapter
        val geofenceApi = retrofit.create(GeofenceInterface::class.java)
        geofenceApi.getGeofenceRequest(apiKey, accessToken).enqueue(object : Callback<JsonElement> {

            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    AppLog.i(this, "Geofence response: " + response.body().toString())
                    Log.i("response", response.body().toString())
                    val jsonElement = response.body().toString()
                    try {
                        val jsonObject = JSONObject(jsonElement)
                        EventBus.post(Events.GeofenceEvent(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (response.code() == 401) {
                    val error = ErrorUtils.parseError(response)
                    val gson = Gson()
                    val json = gson.toJson(error)
                    Log.d("error message", json)
                    try {
                        val jsonObject = JSONObject(json)
                        EventBus.post(Events.GeofenceEvent(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                AppLog.i(this, "Failure: " + t.localizedMessage)
            }


        })

    }


}
