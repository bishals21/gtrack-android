package com.trackomotor.portal.trackomotor.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.squareup.otto.Subscribe
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.adapter.GeofenceListAdapter
import com.trackomotor.portal.trackomotor.controller.request.AlertTask
import com.trackomotor.portal.trackomotor.controller.request.GeofenceTask
import com.trackomotor.portal.trackomotor.model.GeofenceDevice
import com.trackomotor.portal.trackomotor.model.GeofenceModel
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.util.AppText
import com.trackomotor.portal.trackomotor.util.AppUtil
import com.trackomotor.portal.trackomotor.util.DividerItemDecoration
import com.trackomotor.portal.trackomotor.util.RecyclerItemClickListener
import kotlinx.android.synthetic.main.activity_geofence.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class GeofenceActivity : AppCompatActivity(), RecyclerItemClickListener.OnItemClickListener {

    private var geofenceList: MutableList<GeofenceModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geofence)
        EventBus.register(this)

        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar.title = "Geofence"

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(this, R.drawable.divider))
        recyclerView.addOnItemTouchListener(RecyclerItemClickListener(this, this))
        swipeRefreshLayout.setOnRefreshListener { fetchData() }
        fetchData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun fetchData() {
        if (AppUtil.isNetworkConnected(this)) {
            startRefreshing()
            GeofenceTask().geofenceRequest(AppText.API_KEY, UserAccessToken.userToken.token)
        } else {
            stopRefreshing()
            Toast.makeText(this, "No Internet Connection!!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun startRefreshing() {
        swipeRefreshLayout.post { swipeRefreshLayout.isRefreshing = true }
    }

    private fun stopRefreshing() {
        swipeRefreshLayout.isRefreshing = false
        val handler = Handler()
        handler.postDelayed({ swipeRefreshLayout.isRefreshing = false }, 100)
    }

    @Subscribe
    fun GeofenceEvent(event: Events.GeofenceEvent?) {
        stopRefreshing()
        if (event != null) {
            val jsonObject = event.geofenceResponse
            geofenceList = ArrayList<GeofenceModel>()
            try {
                if (jsonObject.getBoolean("status")) {
                    val dataObject = jsonObject.getJSONObject("data")
                    val geofenceArray: JSONArray = dataObject!!.getJSONArray("geofences")
                    for (i in 0 until geofenceArray.length()) {
                        val geofenceDeviceList = ArrayList<GeofenceDevice>()
                        val item = geofenceArray.get(i) as JSONObject

                        val id = item.getInt("id")
                        val name = item.getString("name")
                        val slug = item.getString("slug")
                        val description = item.getString("description")
                        val area = item.getString("area")
                        val type = item.getString("type")
                        val created = item.getString("created")
                        val devicesArray: JSONArray = item.getJSONArray("devices")
                        for (j in 0 until devicesArray.length()) {
                            val deviceItem = devicesArray.get(j) as JSONObject

                            val deviceId = deviceItem.getInt("deviceId")
                            val deviceName = deviceItem.getString("deviceName")
                            val vehicleId = deviceItem.getInt("vehicleId")
                            val targetName = deviceItem.getString("targetName")
                            geofenceDeviceList.add(GeofenceDevice(deviceId, deviceName, vehicleId, targetName))
                        }
                        geofenceList!!.add(GeofenceModel(id, name, slug, description, area, type, created, geofenceDeviceList))
                    }

                    recyclerView.adapter = GeofenceListAdapter(this, geofenceList!!)
                } else if (!jsonObject.getBoolean("status")) {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }

    }

    override fun onItemClick(recyclerView: RecyclerView, view: View, position: Int) {
        val geofenceModel = geofenceList!![position]
        val area = geofenceModel.area
        val title = geofenceModel.name
        val intent = Intent(this, GeofenceDetailActivity::class.java)
        intent.putExtra("area", area)
        intent.putExtra("title", title)
        startActivity(intent)
    }



    override fun onDestroy() {
        super.onDestroy()
        EventBus.unregister(this)
    }

}
