package com.trackomotor.portal.trackomotor.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.model.GeofenceModel
import kotlinx.android.synthetic.main.adapter_geofence_list.view.*


/**
 * Created by yarsha-and01 on 5/22/17.
 */

class GeofenceListAdapter(private val context: Context, internal var list: List<GeofenceModel>) : RecyclerView.Adapter<GeofenceListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_geofence_list, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val area= list[position].area
        val areaSplit = area!!.split(" ")
        holder.geofenceName.text = list[position].name
        holder.geofenceArea.text = areaSplit[0]
        holder.geofenceType.text = list[position].type

        if (list[position].type.equals("OUT")) {
            holder.geofenceType.setTextColor(ContextCompat.getColor(context, R.color.geofenceOut))
        } else if (list[position].type.equals("IN")) {
            holder.geofenceType.setTextColor(ContextCompat.getColor(context, R.color.geofenceIn))
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val geofenceName = view.geofenceName
        val geofenceArea = view.geofenceArea
        val geofenceType = view.geofenceType
    }


}
