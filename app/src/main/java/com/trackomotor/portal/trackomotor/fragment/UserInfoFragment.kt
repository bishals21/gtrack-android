package com.trackomotor.portal.trackomotor.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import kotlinx.android.synthetic.main.fragment_user_info.*

/**
 * Created by bishal21 on 3/7/2018.
 */
class UserInfoFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_user_info, container, false)
        return view

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.with(activity).load(UserAccessToken.userToken.image).into(userImage)
        nameTextView.text = UserAccessToken.userToken.name
        usernameTextview.text = UserAccessToken.userToken.username
        contactTextView.text = UserAccessToken.userToken.contact
        addressTextView.text = UserAccessToken.userToken.address

    }
}