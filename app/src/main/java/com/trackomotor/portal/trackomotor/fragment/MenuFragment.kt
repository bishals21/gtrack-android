package com.trackomotor.portal.trackomotor.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.activity.DeviceMessageActivity
import com.trackomotor.portal.trackomotor.activity.GeofenceActivity
import com.trackomotor.portal.trackomotor.activity.PlaybackChooseActivity
import com.trackomotor.portal.trackomotor.activity.SplashActivity
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.util.PrefUtils
import kotlinx.android.synthetic.main.fragment_menu.*

/**
 * Created by bishal21 on 3/7/2018.
 */
class MenuFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_menu, container, false)
        return view

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnAlert.setOnClickListener {
            val intent = Intent(activity, DeviceMessageActivity::class.java)
            startActivity(intent)
        }

        menuHistory.setOnClickListener {
            val intent = Intent(activity, PlaybackChooseActivity::class.java)
            startActivity(intent)
        }

        menuGeofence.setOnClickListener {
            val intent = Intent(activity, GeofenceActivity::class.java)
            startActivity(intent)
        }

        menuLogout.setOnClickListener {
            UserAccessToken.deleteLoginData()
            PrefUtils.setUserLoggedIn(activity, false);
            val intent = Intent(activity, SplashActivity::class.java)
            startActivity(intent)
        }

    }


}