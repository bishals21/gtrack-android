package com.trackomotor.portal.trackomotor.EventBus


import com.trackomotor.portal.trackomotor.model.DepartmentsVechicle

import org.json.JSONObject

class Events {

    class LoginEvent(val loginResponse: JSONObject)


    class DepartmentsEvent(val departmentsResponse: JSONObject)


    class VehicleEvent(val vehicleResponse: JSONObject)


    class PlaybackEvent(val playbackResponse: JSONObject)

    class VehicleAlert(val alertResponse: JSONObject)

    class NextVehicleAlert(val alertResponse: JSONObject)

    class GeofenceEvent(val geofenceResponse: JSONObject)

    class ErrorEvent(val error: String)


}
