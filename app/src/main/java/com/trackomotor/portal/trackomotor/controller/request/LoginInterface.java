package com.trackomotor.portal.trackomotor.controller.request;


import com.google.gson.JsonElement;
import com.trackomotor.portal.trackomotor.model.LoginRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Bishal on 7/6/2017.
 */

public interface LoginInterface {
    @Headers({
            "Content-Type: application/json"
    })

    @POST("authenticate")
    Call<JsonElement> postLoginRequest(
            @Header("api-key") String apiKey,
            @Body LoginRequest loginRequest
    );
}
