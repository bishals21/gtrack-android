package com.trackomotor.portal.trackomotor.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackomotor.portal.trackomotor.R;
import com.trackomotor.portal.trackomotor.model.VehicleAlert;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by bishal21 on 1/14/2018.
 */

public class DeviceMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<VehicleAlert> list;
    private Context context;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private OnLoadMoreListener onLoadMoreListener;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView recyclerView;
    private boolean isMoreLoading = false;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    public interface OnLoadMoreListener {
        void onLoadMore();
    }


    public DeviceMessageAdapter(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
        list = new ArrayList<>();
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    public void setRecyclerView(RecyclerView mView) {
        recyclerView = mView;
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                if (!isMoreLoading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isMoreLoading = true;
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.adapter_device_message_fragment, parent, false);

            return new MyHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);

            return new ProgressBarViewHolder(v);
        }
    }

    public void addAll(List<VehicleAlert> lst) {
        list.clear();
        list.addAll(lst);
        notifyDataSetChanged();
    }

    public void addItemMore(List<VehicleAlert> lst) {
        list.addAll(lst);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        VehicleAlert vehicleAlert = list.get(position);
        /*String dateTime = vehicleAlert.getTime();
        String[] dateSplit = dateTime.split(" ");*/

        if(holder instanceof MyHolder){
            ((MyHolder) holder).vehicleName.setText(vehicleAlert.getVehicle());
            ((MyHolder) holder).alertType.setText(vehicleAlert.getType());
            ((MyHolder) holder).alertDate.setText(vehicleAlert.getTime().split(" ")[0]);
            ((MyHolder) holder).alertTime.setText(vehicleAlert.getTime().split(" ")[1]);
        }
    }
    public void setMoreLoading(boolean isMoreLoading) {
        this.isMoreLoading = isMoreLoading;
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    list.add(null);
                    notifyItemInserted(list.size() - 1);
                }
            });
        } else {
            list.remove(list.size() - 1);
            notifyItemRemoved(list.size());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    public static class MyHolder extends RecyclerView.ViewHolder {

        TextView vehicleName;
        TextView alertType;
        TextView alertDate;
        TextView alertTime;

        public MyHolder(View itemView) {
            super(itemView);
            vehicleName = itemView.findViewById(R.id.geofenceName);
            alertType = itemView.findViewById(R.id.geofenceArea);
            alertDate = itemView.findViewById(R.id.alertDate);
            alertTime = itemView.findViewById(R.id.alertTime);
        }
    }

    public static class ProgressBarViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar1;

        public ProgressBarViewHolder(View itemView) {
            super(itemView);
            progressBar1 = (ProgressBar) itemView.findViewById(R.id.progressbar);
        }

    }
}
