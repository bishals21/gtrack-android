package com.trackomotor.portal.trackomotor.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object AppUtil {

    var ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ5YXJzaGEiLCJhdWQiOiJBZG1pbiIsImp0aSI6IjlwTEFFWUxuTkxpaUtuSWxQWTJuTEEiLCJpYXQiOjE1MDk5NTM2MDksInVzZXJJZCI6MTUsImNvbXBhbnkiOiJZYXJzaGEiLCJ1c2VybmFtZSI6Imdsb2JhbGltZSIsImV4cCI6MTUxMDU1ODQwOX0.0yv1d1aOCzaF80NMdB1jCLHkVpP9sLyLcIC2EpnLTYY"

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return if (networkInfo == null) {
            false
        } else {
            networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE
        }
    }

}
