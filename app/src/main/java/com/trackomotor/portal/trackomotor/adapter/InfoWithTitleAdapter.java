package com.trackomotor.portal.trackomotor.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.trackomotor.portal.trackomotor.R;
import com.trackomotor.portal.trackomotor.model.InfoWindowData;
import com.trackomotor.portal.trackomotor.model.InfoWindowDataWithTitle;


public class InfoWithTitleAdapter implements InfoWindowAdapter {
    private View popup=null;
    private LayoutInflater inflater=null;

    public InfoWithTitleAdapter(LayoutInflater inflater) {
        this.inflater=inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return(null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        if (popup == null) {
            popup=inflater.inflate(R.layout.info_window_with_title, null);
        }

        TextView titleTv=(TextView)popup.findViewById(R.id.marker_title);
        TextView latLngTv=(TextView)popup.findViewById(R.id.marker_lat_lng);
        TextView addressTv=(TextView)popup.findViewById(R.id.marker_address);
        TextView speedTv=(TextView)popup.findViewById(R.id.marker_speed);

        InfoWindowDataWithTitle info = (InfoWindowDataWithTitle) marker.getTag();
        titleTv.setText(info.getTitle());
        latLngTv.setText("Lat: "+info.getLat()+", Lng: "+info.getLng());
        addressTv.setText("Address: "+info.getAddress());
        speedTv.setText("Speed: "+info.getSpeed()+" km/hr");
        return(popup);
    }
}
