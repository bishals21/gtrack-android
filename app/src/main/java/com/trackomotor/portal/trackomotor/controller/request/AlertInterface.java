package com.trackomotor.portal.trackomotor.controller.request;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Url;

/**
 * Created by bishal21 on 3/11/2018.
 */

public interface AlertInterface {
    @Headers({
            "Content-Type: application/json"
    })

    @GET("company/event-alerts")
    Call<JsonElement> getAlertRequest(
            @Header("api-key") String apiKey,
            @Header("access-token") String accessToken
    );

    @GET
    Call<JsonElement> getNextAlertRequest(
            @Header("api-key") String apiKey,
            @Header("access-token") String accessToken,
            @Url String nextPageUrl
    );
}
