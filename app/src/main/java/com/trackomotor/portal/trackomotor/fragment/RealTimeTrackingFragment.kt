package com.trackomotor.portal.trackomotor.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.util.SparseArray
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.*
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.trackomotor.portal.trackomotor.activity.MainActivity
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.model.DepartmentsVechicle
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.util.BubbleDrawable
import com.trackomotor.portal.trackomotor.util.ServerConnection
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by bishal21 on 3/12/2018.
 */
class RealTimeTrackingFragment: Fragment(), OnMapReadyCallback, ServerConnection.ServerListener {

    private val SERVER_URL: String = UserAccessToken.userToken.web_socket_url+ UserAccessToken.userToken.token
    private var serverConnection: ServerConnection? = null
    private lateinit var mMap: GoogleMap

    private val hashMapMarker = SparseArray<Marker>()
    private val hashMapPolyline = SparseArray<LatLng>()
    private var departmentsVechicleList: List<DepartmentsVechicle>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as MainActivity).supportActionBar!!.title = "Real Time Track"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_real_time_tracking, container, false)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        serverConnection = ServerConnection(SERVER_URL)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.map_menu, menu)
        menu!!.findItem(R.id.normal_map).isChecked = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.getItemId()) {
            R.id.normal_map -> {
                updateMapType(resources.getString(R.string.normal))
                item.setChecked(!item.isChecked())
                return true
            }
            R.id.hybrid_map -> {
                updateMapType(resources.getString(R.string.hybrid))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.satellite_map -> {
                updateMapType(resources.getString(R.string.satellite))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.terrain_map -> {
                updateMapType(resources.getString(R.string.terrain))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.none_map -> {
                updateMapType(resources.getString(R.string.none_map))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.traffic_enabled -> {
                updateTraffic(item.isChecked())
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.building_enabled -> {
                updateBuildings(item.isChecked())
                item.isChecked = !item.isChecked()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun updateMapType(layerName: String) {
        // No toast because this can also be called by the Android framework in onResume() at which
        // point mMap may not be ready yet.
        when (layerName) {
            getString(R.string.normal) -> mMap.mapType = MAP_TYPE_NORMAL
            getString(R.string.hybrid) -> mMap.mapType = MAP_TYPE_HYBRID
            getString(R.string.satellite) -> mMap.mapType = MAP_TYPE_SATELLITE
            getString(R.string.terrain) -> mMap.mapType = MAP_TYPE_TERRAIN
            getString(R.string.none_map) -> mMap.mapType = MAP_TYPE_NONE
            else -> Log.i("LDA", "Error setting layer with name $layerName")
        }
    }

    private fun updateBuildings(isChecked: Boolean) {
        mMap.isBuildingsEnabled = isChecked
    }


    private fun updateTraffic(isChecked: Boolean) {
        mMap.isTrafficEnabled = isChecked
    }

   /* private fun checkReady(): Boolean {
        if (mMap == null) {
            Toast.makeText(activity, R.string.map_not_ready, Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }*/

    override fun onResume() {
        super.onResume()
        serverConnection!!.connect(this)
    }

    override fun onPause() {
        super.onPause()
        serverConnection!!.disconnect()
    }



    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = true
    }

    override fun onNewMessage(message: String) {
        Log.d("WebSocketMessage", message)
        try {
            val jsonObject = JSONObject(message)
            if (jsonObject.has("positions")) {
                val positions = jsonObject.getJSONObject("positions")
                Log.d("Reached Device", "onNewMessage: Reached Here")
                val latitude = positions.getDouble("latitude")
                val longitude = positions.getDouble("longitude")
                val speed = positions.getDouble("speed")
                val deviceId = positions.getInt("deviceId")
                updateLocation(LatLng(latitude, longitude), speed, deviceId)
            } /*else if (jsonObject.has("devices")) {
                val devices = jsonObject.getJSONObject("devices")
                val deviceId = devices.getInt("id")
                val status = devices.getString("status")

                updateStatus(deviceId, status)
            }*/
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }


    private fun updateLocation(location: LatLng, speed: Double?, deviceId: Int) {
        activity.runOnUiThread {
            var deviceName = ""
            var overSpeedLimit = 0.0
            val marker: Marker
            val polyline: Polyline


            for (i in departmentsVechicleList!!.indices) {
                if (departmentsVechicleList!![i].deviceId == deviceId) {
                    deviceName = departmentsVechicleList!![i].deviceName!!
                    overSpeedLimit = departmentsVechicleList!![i].overSpeedLimit!!.toDouble()
                }
            }
            if (hashMapMarker.get(deviceId) != null) {
                marker = hashMapMarker.get(deviceId)
                marker.setPosition(location)
                hashMapMarker.put(deviceId, marker)
            } else {
                val tv = this.layoutInflater.inflate(R.layout.marker_dialog, null, false) as LinearLayout
                val textView = tv.findViewById<View>(R.id.marker_text) as TextView
                val myBubble = BubbleDrawable(BubbleDrawable.CENTER)
                myBubble.setPointerAlignment(BubbleDrawable.CENTER)
                myBubble.setPadding(10, 10, 10, 10)
                textView.background = myBubble
               /* textView.setBackgroundDrawable(myBubble)*/
                textView.text = deviceName
                tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
                tv.layout(0, 0, tv.measuredWidth, tv.measuredHeight)
                tv.isDrawingCacheEnabled = true
                tv.buildDrawingCache()
                val bm = tv.drawingCache

                marker = mMap.addMarker(MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(bm)))
                hashMapMarker.put(deviceId, marker)


            }
            if (hashMapPolyline.get(deviceId) != null) {
                if (speed!! > overSpeedLimit) {
                    val latLng = hashMapPolyline.get(deviceId)
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.RED).clickable(true).add(latLng, location).geodesic(true).startCap(RoundCap()))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "R"
                    hashMapPolyline.put(deviceId, location)
                } else {
                    val latLng = hashMapPolyline.get(deviceId)
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.GREEN).clickable(true).add(latLng, location).geodesic(true).startCap(RoundCap()))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "G"
                    hashMapPolyline.put(deviceId, location)
                }


            } else {
                if (speed!! > overSpeedLimit) {
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.RED).clickable(true).add(location))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "R"
                    hashMapPolyline.put(deviceId, location)

                } else {
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.GREEN).clickable(true).add(location))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "G"
                    hashMapPolyline.put(deviceId, location)

                }

            }


        }
    }

    override fun onStatusChange(status: ServerConnection.ConnectionStatus) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}