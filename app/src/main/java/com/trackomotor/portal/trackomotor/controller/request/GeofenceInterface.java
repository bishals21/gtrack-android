package com.trackomotor.portal.trackomotor.controller.request;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Url;

/**
 * Created by bishal21 on 3/11/2018.
 */

public interface GeofenceInterface {
    @Headers({
            "Content-Type: application/json"
    })

    @GET("company/geofences")
    Call<JsonElement> getGeofenceRequest(
            @Header("api-key") String apiKey,
            @Header("access-token") String accessToken
    );

}
