package com.trackomotor.portal.trackomotor.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.model.DepartmentsVechicle
import com.trackomotor.portal.trackomotor.model.MenuItem
import kotlinx.android.synthetic.main.adapter_menu.view.*
import kotlinx.android.synthetic.main.adapter_vehicle_list.view.*


/**
 * Created by yarsha-and01 on 5/22/17.
 */

class MenuAdapter(private val context: Context, internal var list: List<MenuItem>) : RecyclerView.Adapter<MenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_menu, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val id = context.resources.getIdentifier(list[position].menuImage, "drawable", context.packageName)
        holder.menuImage.setImageResource(id)
        holder.menuTitle.text = list[position].menuTitle

    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val menuImage = view.menuImage
        val menuTitle = view.menuTitle

    }


}
