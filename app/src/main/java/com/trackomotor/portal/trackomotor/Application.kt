package com.trackomotor.portal.trackomotor

import android.util.Log
import com.activeandroid.ActiveAndroid

/**
 * Created by bishal21 on 3/9/2018.
 */
class Application : com.activeandroid.app.Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        try {
            ActiveAndroid.initialize(this)
            Log.i(TAG, "initialized")
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    override fun onTerminate() {
        super.onTerminate()
        ActiveAndroid.dispose()
    }

    companion object {

        val TAG = Application::class.java.simpleName


        @get:Synchronized
        var instance: Application? = null
            private set
    }
}