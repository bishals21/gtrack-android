package com.trackomotor.portal.trackomotor.controller.request;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

/**
 * Created by bishal21 on 3/13/2018.
 */

public interface VehicleInterface {
    @Headers({
            "Content-Type: application/json"
    })

    @GET("company/departments")
    Call<JsonElement> getVehicleListRequest(
            @Header("api-key") String apiKey,
            @Header("access-token") String accessToken
    );
}
