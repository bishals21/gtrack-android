package com.trackomotor.portal.trackomotor.EventBus

import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer

object EventBus {

    private var eventBus: Bus? = null

    private val bus: Bus
        get() {
            if (eventBus == null) {
                eventBus = Bus(ThreadEnforcer.ANY)
            }

            return eventBus as Bus
        }

    /**
     * Wrapper method to register event
     */
    fun register(event: Any) {
        bus.register(event)
    }

    /**
     * Wrapper method to unregister event
     */
    fun unregister(event: Any) {
        bus.unregister(event)
    }

    /**
     * Wrapper method to post event
     */
    fun post(event: Any) {
        bus.post(event)
    }
}
