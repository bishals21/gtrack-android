package com.trackomotor.portal.trackomotor.activity

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.PopupMenu
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.otto.Subscribe
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.adapter.InfoAdapter
import com.trackomotor.portal.trackomotor.controller.request.PlaybackTask
import com.trackomotor.portal.trackomotor.model.*
import com.trackomotor.portal.trackomotor.util.AppText
import com.trackomotor.portal.trackomotor.util.AppUtil
import kotlinx.android.synthetic.main.activity_maps.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var alertDialog: AlertDialog? = null

    private var positionModelList: MutableList<PositionsModel>? = null
    private var coordinates: ArrayList<LatLng>? = null
    internal var polyline: Polyline? = null
    internal var markerStart: Marker? = null
    internal var markerEnd: Marker? = null

    private var counter = 0

    private var timer: Timer? = null

    private var vehicleId: Int? = null
    private var fromDate: String? = null
    private var toDate: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        EventBus.register(this)


        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar.title = "Watch Routes"
        initDialog()


        if (intent.extras != null) {
            vehicleId = intent.getIntExtra("vehicleId", 0)
            fromDate = intent.getStringExtra("fromDate")
            toDate = intent.getStringExtra("toDate")
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        btnPlay.setOnCheckedChangeListener { _, isChecked ->
            if (positionModelList != null) {
                if (isChecked) {
                    scheduleTimer(1)
                } else {
                    stopTimer()
                }
            }

        }


        btnSpeed.setOnClickListener() { view ->
            showSpeedPopup(view)
        }

        btnRewind.setOnClickListener() {
            if (timer != null) {
                scheduleRewindTimer(6)
            }

        }

        btnForward.setOnClickListener() {
            if (timer != null) {
                scheduleTimer(6)
            }

        }
        fetchData()

    }

    private fun showSpeedPopup(view: View) {
        val popup: PopupMenu?
        popup = PopupMenu(this, view)
        popup.inflate(R.menu.speed_menu)
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.speedOne -> {
                    if (timer != null) {
                        scheduleTimer(1)
                        btnSpeed.text = "1.0X"
                    }


                }
                R.id.speedTwo -> {
                    if (timer != null) {
                        scheduleTimer(2)
                        btnSpeed.text = "2.0X"
                    }


                }
                R.id.speedThree -> {
                    if (timer != null) {
                        scheduleTimer(3)
                        btnSpeed.text = "3.0X"
                    }


                }
                R.id.speedFour -> {
                    if (timer != null) {
                        scheduleTimer(4)
                        btnSpeed.text = "4.0X"
                    }


                }
            }
            true
        })
        popup.show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopTimer()
    }

    //XNgH06r0

    private fun initDialog() {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.progressbar_dialog, null)
        alertDialogBuilder.setView(view)
        alertDialog = alertDialogBuilder.create()
        alertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val infoAdapter = InfoAdapter(layoutInflater)
        mMap = googleMap
        mMap.setInfoWindowAdapter(infoAdapter)

    }


    private fun drawPolyLine() {
        coordinates = ArrayList()
        this.runOnUiThread {
            for (i in positionModelList!!.indices) {
                val positionModel = positionModelList!!.get(i)
                val lat = java.lang.Double.parseDouble(positionModel.latitude)
                val lng = java.lang.Double.parseDouble(positionModel.longitude)
                coordinates!!.add(LatLng(lat, lng))
            }

            if (coordinates!!.size > 2) {
                val latLng = coordinates!![0]

                val address = positionModelList!![0].address
                val speed = positionModelList!![0].speed!!.toFloat()

                polyline = mMap.addPolyline(PolylineOptions().color(Color.GREEN).addAll(coordinates).geodesic(true))
                markerStart = mMap.addMarker(MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_arrow))

                        .anchor(0.5f, 0.5f)
                        )
                markerStart!!.tag = InfoWindowData(String.format("%.04f", latLng.latitude), String.format("%.04f", latLng.longitude), address, java.lang.Math.round(1.852 * speed).toString())
                markerStart!!.showInfoWindow()
                markerStart!!.setInfoWindowAnchor(0.5f,0.7f)
                markerEnd = mMap.addMarker(MarkerOptions().title("End").position(coordinates!![coordinates!!.size - 1]))
            }

            if (coordinates!!.size > 1) {
                val cameraPosition = CameraPosition.Builder()
                        .target(com.google.android.gms.maps.model.LatLng(java.lang.Double.parseDouble(positionModelList!![0].latitude), java.lang.Double.parseDouble(positionModelList!![0].longitude)))
                        .zoom(16f)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }


        }
    }


    private fun scheduleTimer(duration: Int) {
        stopTimer()
        timer = Timer("alertTimer", true)
        val markerPlacementTask = MarkerPlacement()
        timer!!.schedule(markerPlacementTask, 0, (1000 / duration).toLong())
    }

    private fun scheduleRewindTimer(duration: Int) {
        stopTimer()
        timer = Timer("alertTimer", true)
        val rewindTimerTask = RewindMarkerPosition()
        timer!!.schedule(rewindTimerTask, 0, (1000 / duration).toLong())
    }

    private inner class MarkerPlacement : TimerTask() {
        override fun run() {
            runOnUiThread {
                if (counter < coordinates!!.size) {
                    val latLng = coordinates!![counter]

                    val address = positionModelList!![counter].address
                    val speed = positionModelList!![counter].speed!!.toFloat()
                    Log.d("LatLng", latLng.toString())
                    markerStart!!.position = latLng

                    markerStart!!.setAnchor(0.5f, 0.5f)
                    markerStart!!.tag = InfoWindowData(String.format("%.04f", latLng.latitude), String.format("%.04f", latLng.longitude),  address, java.lang.Math.round(1.852 * speed).toString())
                    markerStart!!.showInfoWindow()
                    markerStart!!.setInfoWindowAnchor(0.5f,0.7f)
                    val cameraPosition = CameraPosition.Builder()
                            .target(latLng)
                            .zoom(16f)
                            .build()
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    progressBar.progress = counter + 1
                    ++counter
                } else {
                    stopTimer()
                    btnPlay.isChecked = false
                }


            }


        }
    }

    private inner class RewindMarkerPosition : TimerTask() {
        override fun run() {
            runOnUiThread {
                if (counter > 0) {
                    val latLng = coordinates!![counter - 1]

                    val address = positionModelList!![counter - 1].address
                    val speed = positionModelList!![counter - 1].speed!!.toFloat()
                    Log.d("LatLng", latLng.toString())
                    markerStart!!.position = latLng

                    markerStart!!.setAnchor(0.5f, 0.5f)
                    markerStart!!.tag = InfoWindowData(String.format("%.04f", latLng.latitude), String.format("%.04f", latLng.longitude),  address, java.lang.Math.round(1.852 * speed).toString())
                    markerStart!!.showInfoWindow()
                    markerStart!!.setInfoWindowAnchor(0.5f, 0.5f)
                    val cameraPosition = CameraPosition.Builder()
                            .target(latLng)
                            .zoom(16f)
                            .build()
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    progressBar.progress = counter - 1
                    --counter
                } else {
                    stopTimer()
                    btnPlay.isChecked = false
                }


            }


        }
    }


    private fun stopTimer() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }


    private fun fetchData() {
        if (AppUtil.isNetworkConnected(this)) {
            alertDialog!!.show()
            PlaybackTask().getPosition(AppText.API_KEY, UserAccessToken.userToken.token, vehicleId!!, fromDate!!, toDate!!)
        } else {
            alertDialog!!.dismiss()
            Snackbar.make(constraintLayout, "No Internet Connection!!!", Snackbar.LENGTH_SHORT).show()
        }
    }


    @Subscribe
    fun PlaybackEvent(event: Events.PlaybackEvent?) {
        alertDialog!!.dismiss()
        if (event != null) {
            val playbackObject = event.playbackResponse
            try {
                if (playbackObject.getString("status").equals("success")) {
                    positionModelList = ArrayList<PositionsModel>()
                    val positionArray = playbackObject.getJSONArray("positions")
                    for (i in 0 until positionArray.length()) {
                        val insideObject = positionArray.get(i) as JSONObject
                        val overspeed = insideObject.getBoolean("overspeed")
                        val latitude = insideObject.getString("latitude")
                        val longitude = insideObject.getString("longitude")

                        val speed = insideObject.getString("speed")

                        val address = insideObject.getString("address")

                        positionModelList!!.add(PositionsModel(overspeed, latitude, longitude,address, speed))

                    }

                    if (positionModelList != null) {
                        drawPolyLine()
                        progressBar.max = positionModelList!!.size
                    }


                } else {
                    Toast.makeText(this, playbackObject.getString("status"), Toast.LENGTH_LONG).show()
                    finish()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }
}
