package com.trackomotor.portal.trackomotor.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bishal21 on 2/12/2018.
 */

/*
"id": "2",
        "device": "1",
        "device_time": "2018-01-19 01:00:29",
        "lat": "27.745275",
        "lng": "85.33607777777779",
        "speed": "0",
        "distance": "0",
        "day": "2018-01-19",
        "mileage": "10",
        "vehicle": "409",
        "address": "211 Pipal Marg, Kathmandu, Central Development Region, NP",
        "vehicleId": "1"*/
class PositionModel() : Parcelable {

    var id: String? = null
    var device: String? = null
    var deviceTime: String? = null
    var lat: String? = null
    var lng: String? = null
    var speed: String? = null
    var distance: String? = null
    var day: String? = null
    var mileage: String? = null
    var vehicle: String? = null
    var address: String? = null
    var vehicleId: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        device = parcel.readString()
        deviceTime = parcel.readString()
        lat = parcel.readString()
        lng = parcel.readString()
        speed = parcel.readString()
        distance = parcel.readString()
        day = parcel.readString()
        mileage = parcel.readString()
        vehicle = parcel.readString()
        address = parcel.readString()
        vehicleId = parcel.readString()
    }

    constructor(id: String, device: String, deviceTime: String, lat: String, lng: String, speed: String, distance: String, day: String, mileage: String, vehicle: String, address: String, vehicleId: String) : this() {
        this.id = id
        this.device = device
        this.deviceTime = deviceTime
        this.lat = lat
        this.lng = lng
        this.speed = speed
        this.distance = distance
        this.day = day
        this.mileage = mileage
        this.vehicle = vehicle
        this.address = address
        this.vehicleId = vehicleId
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(device)
        parcel.writeString(deviceTime)
        parcel.writeString(lat)
        parcel.writeString(lng)
        parcel.writeString(speed)
        parcel.writeString(distance)
        parcel.writeString(day)
        parcel.writeString(mileage)
        parcel.writeString(vehicle)
        parcel.writeString(address)
        parcel.writeString(vehicleId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PositionModel> {
        override fun createFromParcel(parcel: Parcel): PositionModel {
            return PositionModel(parcel)
        }

        override fun newArray(size: Int): Array<PositionModel?> {
            return arrayOfNulls(size)
        }
    }

}
