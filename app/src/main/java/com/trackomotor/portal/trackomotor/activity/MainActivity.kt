package com.trackomotor.portal.trackomotor.activity

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.util.SparseArray
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.otto.Subscribe
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.adapter.InfoWithTitleAdapter
import com.trackomotor.portal.trackomotor.adapter.NavigationViewPagerAdapter
import com.trackomotor.portal.trackomotor.adapter.VehicleListAdapter
import com.trackomotor.portal.trackomotor.controller.request.VehicleTask
import com.trackomotor.portal.trackomotor.model.DepartmentsVechicle
import com.trackomotor.portal.trackomotor.model.InfoWindowDataWithTitle
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.util.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback, ServerConnection.ServerListener, Toolbar.OnMenuItemClickListener, RecyclerItemClickListener.OnItemClickListener {


    private val SERVER_URL: String = UserAccessToken.userToken.web_socket_url + UserAccessToken.userToken.token
    //private val SERVER_URL: String = "ws://103.198.9.214:8080/gps-server/api/socket/" + UserAccessToken.userToken.token
    private var serverConnection: ServerConnection? = null
    private lateinit var mMap: GoogleMap

    private val hashMapMarker = SparseArray<Marker>()
    private val hashMapPolyline = SparseArray<LatLng>()
    private var departmentsVechicleList: MutableList<DepartmentsVechicle>? = null
    private lateinit var vehicleListAdapter: VehicleListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        EventBus.register(this)

        toolbar.inflateMenu(R.menu.map_menu)
        toolbar.menu.findItem(R.id.normal_map).isChecked = true
        toolbar.setOnMenuItemClickListener(this)
        toolbar.setNavigationOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }

        setViewPager()
        fetchDepartmentVehicle()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        serverConnection = ServerConnection(SERVER_URL)


    }

    private fun fetchDepartmentVehicle() {
        if (AppUtil.isNetworkConnected(this)) {
            Log.i("network", "connected")
            VehicleTask().getVehicleList(AppText.API_KEY, UserAccessToken.userToken.token)
        } else {
            Toast.makeText(this, "Internet is not connected", Toast.LENGTH_LONG).show()
        }
    }

    @Subscribe
    fun DepartmentsEvent(event: Events.DepartmentsEvent?) {
        if (event != null) {
            try {
                val jsonObject = event.departmentsResponse
                if (jsonObject.getBoolean("status")) {
                    val data = jsonObject.getJSONObject("data")
                    val departments = data.getJSONArray("departments")
                    departmentsVechicleList = ArrayList<DepartmentsVechicle>()
                    for (i in 0 until departments.length()) {
                        val dataItem = departments.getJSONObject(i)
                        val name = dataItem.getString("name")
                        val vehicleArray = dataItem.getJSONArray("vehicles")
                        for (j in 0 until vehicleArray.length()) {
                            val item = vehicleArray.getJSONObject(j)
                            val number = item.getString("number")
                            val code = item.getString("code")
                            val deviceId = item.getInt("deviceId")
                            val deviceUniqueId = item.getString("deviceUniqueId")
                            val deviceName = item.getString("deviceName")
                            val deviceStatus = item.getString("deviceStatus")
                            val vehicleType = item.getString("vehicleType")
                            val vehicleTypeImage = item.getString("vehicleTypeImage")
                            val overSpeedLimit = item.getInt("overSpeedLimit")
                            val lastUpdate = item.getString("lastUpdate")
                            val longitude = item.getString("longitude")
                            val latitude = item.getString("latitude")
                            val address = item.getString("address")
                            val speed = item.getString("speed")
                            val ignition = item.getString("ignition")
                            val charge = item.getString("charge")
                            val battery = item.getString("battery")
                            val rssi = item.getString("rssi")
                            val course = item.getString("course")
                            val alarm = item.getString("alarm")
                            val lastPositionTime = item.getString("lastPositionTime")
                            val event = item.getString("event")
                            val lastEventTime = item.getString("lastEventTime")
                            departmentsVechicleList!!.add(DepartmentsVechicle(name, number, code, deviceId, deviceUniqueId, deviceName, deviceStatus, vehicleType, vehicleTypeImage, overSpeedLimit, lastUpdate, longitude, latitude, address, speed, ignition, charge, battery, rssi, course, alarm, lastPositionTime, event, lastEventTime))
                        }

                    }

                    vehicleListRecyclerView!!.layoutManager = LinearLayoutManager(this)
                    vehicleListRecyclerView!!.setHasFixedSize(true)
                    val sectionItemDecoration = RecyclerSectionItemDecoration(resources.getDimensionPixelSize(R.dimen.recycler_section_header_height),
                            true,
                            getSectionCallback(departmentsVechicleList!!))
                    vehicleListRecyclerView!!.addItemDecoration(sectionItemDecoration)
                    vehicleListRecyclerView!!.itemAnimator = DefaultItemAnimator()
                    vehicleListRecyclerView!!.addOnItemTouchListener(RecyclerItemClickListener(this, this))
                    vehicleListAdapter = VehicleListAdapter(this, departmentsVechicleList!!)
                    vehicleListRecyclerView!!.adapter = vehicleListAdapter


                } else if (!jsonObject.getBoolean("status")) {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }


        }

    }

    private fun getSectionCallback(hotels: List<DepartmentsVechicle>): RecyclerSectionItemDecoration.SectionCallback {
        return object : RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return position == 0 || hotels.get(position)
                        .department !== hotels.get(position - 1).department

            }

            override fun getSectionHeader(position: Int): String {
                return hotels.get(position)
                        .department.toString()
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_vehicle_list -> {
                if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
                    drawer_layout.closeDrawer(GravityCompat.END)
                } else {
                    drawer_layout.openDrawer(GravityCompat.END)
                }
                return true
            }
            R.id.normal_map -> {
                updateMapType(resources.getString(R.string.normal))
                item.setChecked(!item.isChecked())
                return true
            }
            R.id.hybrid_map -> {
                updateMapType(resources.getString(R.string.hybrid))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.satellite_map -> {
                updateMapType(resources.getString(R.string.satellite))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.terrain_map -> {
                updateMapType(resources.getString(R.string.terrain))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.none_map -> {
                updateMapType(resources.getString(R.string.none_map))
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.traffic_enabled -> {
                updateTraffic(item.isChecked())
                item.isChecked = !item.isChecked()
                return true
            }
            R.id.building_enabled -> {
                updateBuildings(item.isChecked())
                item.isChecked = !item.isChecked()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun updateMapType(layerName: String) {
        // No toast because this can also be called by the Android framework in onResume() at which
        // point mMap may not be ready yet.
        when (layerName) {
            getString(R.string.normal) -> mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            getString(R.string.hybrid) -> mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
            getString(R.string.satellite) -> mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
            getString(R.string.terrain) -> mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
            getString(R.string.none_map) -> mMap.mapType = GoogleMap.MAP_TYPE_NONE
            else -> Log.i("LDA", "Error setting layer with name $layerName")
        }
    }

    private fun updateBuildings(isChecked: Boolean) {
        mMap.isBuildingsEnabled = isChecked
    }


    private fun updateTraffic(isChecked: Boolean) {
        mMap.isTrafficEnabled = isChecked
    }

    override fun onResume() {
        super.onResume()
        serverConnection!!.connect(this)
    }

    override fun onPause() {
        super.onPause()
        serverConnection!!.disconnect()
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        mMap.uiSettings.isZoomControlsEnabled = true
        val infoAdapter = InfoWithTitleAdapter(layoutInflater)
        mMap.setInfoWindowAdapter(infoAdapter)
    }

    override fun onNewMessage(message: String) {
        Log.d("WebSocketMessage", message)
        try {
            val jsonObject = JSONObject(message)
            if (jsonObject.has("positions")) {
                val positions = jsonObject.getJSONObject("positions")
                Log.d("Reached Device", "onNewMessage: Reached Here")
                val latitude = positions.getDouble("latitude")
                val longitude = positions.getDouble("longitude")
                val speed = positions.getDouble("speed")
                val deviceId = positions.getInt("deviceId")
                val course = positions.getString("course")
                val address = positions.getString("address")
                if (departmentsVechicleList != null) {
                    updateLocation(LatLng(latitude, longitude), speed, deviceId, course.toFloat(), address)
                }

            } else if (jsonObject.has("devices")) {
                val devices = jsonObject.getJSONObject("devices")
                val deviceId = devices.getInt("id")
                val status = devices.getString("status")
                if (departmentsVechicleList != null) {
                    updateDeviceStatus(deviceId, status)
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }


    private fun setViewPager() {
        val adapter = NavigationViewPagerAdapter(supportFragmentManager)
        view_pager.adapter = adapter
        view_pager.setOnTouchListener { v, event ->
            /* Log.e("View Pager", "onTouchPager:" + event.toString())*/
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN ->
                    // Disallow Drawer to intercept touch events.
                    drawer_layout.requestDisallowInterceptTouchEvent(true)
                MotionEvent.ACTION_CANCEL ->
                    // Allow Drawer to intercept touch events.
                    drawer_layout.requestDisallowInterceptTouchEvent(false)
            }

            // Handle viewPager touch events.
            v.onTouchEvent(event)
            true
        }
        tab_dot.setupWithViewPager(view_pager)
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    private fun updateDeviceStatus(deviceId: Int, deviceStatus: String) {

        for (i in 0 until departmentsVechicleList!!.size) {
            if (deviceId == departmentsVechicleList!![i].deviceId) {
                val departmentVehicle: DepartmentsVechicle = departmentsVechicleList!![i]
                departmentVehicle.deviceStatus = deviceStatus
                vehicleListAdapter.notifyDataSetChanged()
            }
        }


    }

    private fun updateLocation(location: LatLng, speed: Double, deviceId: Int, course: Float, address: String) {
        this.runOnUiThread {
            var deviceName = ""
            var overSpeedLimit = 0.0
            val marker: Marker
            val polyline: Polyline


            for (i in departmentsVechicleList!!.indices) {
                if (departmentsVechicleList!![i].deviceId == deviceId) {
                    deviceName = departmentsVechicleList!![i].deviceName!!
                    overSpeedLimit = departmentsVechicleList!![i].overSpeedLimit!!.toDouble()
                }
            }
            if (hashMapMarker.get(deviceId) != null) {
                marker = hashMapMarker.get(deviceId)
//                val tv = this.layoutInflater.inflate(R.layout.marker_dialog, null, false) as LinearLayout
//                val linearLayout = tv.findViewById<View>(R.id.linearLayout) as LinearLayout
//                val titleTextView = tv.findViewById<View>(R.id.marker_text) as TextView
//                //val eventTextView = tv.findViewById<View>(R.id.marker_event) as TextView
//                val latLngTextView = tv.findViewById<View>(R.id.marker_lat_lng) as TextView
//                val addressTextView = tv.findViewById<View>(R.id.marker_address) as TextView
//                val speedTextView = tv.findViewById<View>(R.id.marker_speed) as TextView
//                val imageView = tv.findViewById<View>(R.id.marker_image) as ImageView
//                imageView.rotation = course
//                val myBubble = BubbleDrawable(BubbleDrawable.CENTER)
//                myBubble.setPointerAlignment(BubbleDrawable.CENTER)
//                myBubble.setPadding(10, 10, 10, 10)
//                linearLayout.background = myBubble
//
//                titleTextView.text = deviceName
//                latLngTextView.text = "Lat: " + String.format("%.04f",location.latitude) + ", Lng: " + String.format("%.04f",location.longitude)
//                addressTextView.text = "Address: "+address
//                speedTextView.text = "Speed: "+ java.lang.Math.round(1.852*speed) + " km/hr"
//
//                tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
//                tv.layout(0, 0, tv.measuredWidth, tv.measuredHeight)
//                tv.isDrawingCacheEnabled = true
//                tv.buildDrawingCache()
//                val bm = tv.drawingCache
//                marker.setIcon(BitmapDescriptorFactory.fromBitmap(bm))
//                marker.position = location
//                marker.setAnchor(0.5f, 0.9f)
                marker.position = location
                marker.rotation = course
                marker.setAnchor(0.5f, 0.5f)
                marker.tag = InfoWindowDataWithTitle(deviceName, String.format("%.04f", location.latitude), String.format("%.04f", location.longitude), address, java.lang.Math.round(1.852 * speed).toString())
                marker.showInfoWindow()
                marker.setInfoWindowAnchor(0.5f, 0.7f)
                hashMapMarker.put(deviceId, marker)
            } else {
//                val tv = this.layoutInflater.inflate(R.layout.marker_dialog, null, false) as LinearLayout
//                val linearLayout = tv.findViewById<View>(R.id.linearLayout) as LinearLayout
//                val titleTextView = tv.findViewById<View>(R.id.marker_text) as TextView
//                //val eventTextView = tv.findViewById<View>(R.id.marker_event) as TextView
//                val latLngTextView = tv.findViewById<View>(R.id.marker_lat_lng) as TextView
//                val addressTextView = tv.findViewById<View>(R.id.marker_address) as TextView
//                val speedTextView = tv.findViewById<View>(R.id.marker_speed) as TextView
//                val imageView = tv.findViewById<View>(R.id.marker_image) as ImageView
//                imageView.rotation = course
//                val myBubble = BubbleDrawable(BubbleDrawable.CENTER)
//                myBubble.setPointerAlignment(BubbleDrawable.CENTER)
//                myBubble.setPadding(10, 10, 10, 10)
//                linearLayout.background = myBubble
//
//                titleTextView.text = deviceName
//                latLngTextView.text = "Lat: " + String.format("%.04f",location.latitude) + ", Lng: " + String.format("%.04f",location.longitude)
//                addressTextView.text = "Address: "+address
//                speedTextView.text = "Speed: "+java.lang.Math.round(1.852*speed) + " km/hr"
//                tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
//                tv.layout(0, 0, tv.measuredWidth, tv.measuredHeight)
//                tv.isDrawingCacheEnabled = true
//                tv.buildDrawingCache()
//                val bm = tv.drawingCache
//
//                marker = mMap.addMarker(MarkerOptions().position(location)
//                        .icon(BitmapDescriptorFactory.fromBitmap(bm)))
                marker = mMap.addMarker(com.google.android.gms.maps.model.MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_arrow))
                        .position(location)
                        .rotation(course)
                        .anchor(0.5f, 0.5f))
                marker.tag = InfoWindowDataWithTitle(deviceName, String.format("%.04f", location.latitude), String.format("%.04f", location.longitude), address, Math.round(1.852 * speed).toString())
                marker.showInfoWindow()
                marker.setInfoWindowAnchor(0.5f, 0.7f)
                hashMapMarker.put(deviceId, marker)


            }
            if (hashMapPolyline.get(deviceId) != null) {
                if (speed> overSpeedLimit) {
                    val latLng = hashMapPolyline.get(deviceId)
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.RED).clickable(true).add(latLng, location).geodesic(true).startCap(RoundCap()))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "R"
                    hashMapPolyline.put(deviceId, location)
                } else {
                    val latLng = hashMapPolyline.get(deviceId)
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.GREEN).clickable(true).add(latLng, location).geodesic(true).startCap(RoundCap()))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "G"
                    hashMapPolyline.put(deviceId, location)
                }


            } else {
                if (speed > overSpeedLimit) {
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.RED).clickable(true).add(location))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "R"
                    hashMapPolyline.put(deviceId, location)

                } else {
                    polyline = mMap.addPolyline(PolylineOptions().color(Color.GREEN).clickable(true).add(location))
                    polyline.jointType = JointType.ROUND
                    polyline.tag = "G"
                    hashMapPolyline.put(deviceId, location)

                }

            }


        }
    }

    override fun onStatusChange(status: ServerConnection.ConnectionStatus) {
        Log.d("Socket Connection", serverConnection.toString())
    }

    private fun addMarker(deviceId: Int, deviceName: String, location: LatLng, course: Float, address: String, speed: Float) {
        val marker: Marker = mMap.addMarker(MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_arrow))
                .position(location)
                .rotation(course)
                .anchor(0.5f, 0.5f))
//        val tv = this.layoutInflater.inflate(R.layout.marker_dialog, null, false) as LinearLayout
//        val linearLayout = tv.findViewById<View>(R.id.linearLayout) as LinearLayout
//        val titleTextView = tv.findViewById<View>(R.id.marker_text) as TextView
//        //val eventTextView = tv.findViewById<View>(R.id.marker_event) as TextView
//        val latLngTextView = tv.findViewById<View>(R.id.marker_lat_lng) as TextView
//        val addressTextView = tv.findViewById<View>(R.id.marker_address) as TextView
//        val speedTextView = tv.findViewById<View>(R.id.marker_speed) as TextView
//        val imageView = tv.findViewById<View>(R.id.marker_image) as ImageView
//        imageView.rotation = course
//        val myBubble = BubbleDrawable(BubbleDrawable.CENTER)
//        myBubble.setPointerAlignment(BubbleDrawable.CENTER)
//        myBubble.setPadding(10, 10, 10, 10)
//        linearLayout.background = myBubble
//
//        titleTextView.text = deviceName
//        latLngTextView.text = "Lat: " + String.format("%.04f", location.latitude) + ", Lng: " + String.format("%.04f", location.longitude)
//        addressTextView.text = "Address: " + address
//        speedTextView.text = "Speed: " + java.lang.Math.round(1.852 * speed) + " km/hr"
//        tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
//        tv.layout(0, 0, tv.measuredWidth, tv.measuredHeight)
//        tv.isDrawingCacheEnabled = true
//        tv.buildDrawingCache()
//        val bm = tv.drawingCache
//
//        marker = mMap.addMarker(MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(bm)))
        marker.tag = InfoWindowDataWithTitle(deviceName, String.format("%.04f", location.latitude), String.format("%.04f", location.longitude), address, Math.round(1.852 * speed).toString())
        marker.showInfoWindow()
        marker.setInfoWindowAnchor(0.5f, 0.7f)
        hashMapMarker.put(deviceId, marker)
        val cameraPosition = CameraPosition.Builder()
                .target(location)
                .zoom(16f)
                .build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

    }

    override fun onItemClick(recyclerView: RecyclerView, view: View, position: Int) {
        val departmentsVechicle: DepartmentsVechicle = departmentsVechicleList!!.get(position)
        val deviceId = departmentsVechicle.deviceId
        val deviceName = departmentsVechicle.deviceName!!
        val lat = java.lang.Double.parseDouble(departmentsVechicle.latitude)
        val lng = java.lang.Double.parseDouble(departmentsVechicle.longitude)
        val location = LatLng(lat, lng)
        val course = departmentsVechicle.course!!
        val address = departmentsVechicle.address!!
        val speed = departmentsVechicle.speed!!
        if (hashMapMarker.get(deviceId!!) != null) {
            val marker = hashMapMarker.get(deviceId)
            val markerPosition = marker.position
            val cameraPosition = CameraPosition.Builder()
                    .target(markerPosition)
                    .zoom(16f)
                    .build()
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        } else {
            Toast.makeText(this, "Device hasn't moved.", Toast.LENGTH_SHORT).show()
            addMarker(deviceId, deviceName, location, course.toFloat(), address, speed.toFloat())
        }
        drawer_layout.closeDrawer(GravityCompat.END);
    }
}
