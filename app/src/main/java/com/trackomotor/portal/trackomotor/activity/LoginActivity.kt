package com.trackomotor.portal.trackomotor.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.Toast
import com.squareup.otto.Subscribe
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.controller.request.LoginTask
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.util.AppText
import com.trackomotor.portal.trackomotor.util.AppUtil
import com.trackomotor.portal.trackomotor.util.PrefUtils
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        EventBus.register(this)

        btnLogin.setOnClickListener {
            if (isValidatedValues()) {
                postLogin()
            }
        }

    }

    private fun isValidatedValues(): Boolean {
        var isChecked = true
        val username: String = etUsername.text.toString()
        val password: String = etPassword.text.toString()

        if (username.isEmpty()) {
            Snackbar.make(coordinator, "Username cannot be empty", Snackbar.LENGTH_SHORT).show()
            isChecked = false
        }

        if (password.isEmpty()) {
            Snackbar.make(coordinator, "Password cannot be empty", Snackbar.LENGTH_SHORT).show()
            isChecked = false
        }

        return isChecked
    }

    private fun postLogin() {
        if (AppUtil.isNetworkConnected(this)) {
            LoginTask().loginRequest(etUsername.text.toString(), etPassword.text.toString(), AppText.API_KEY)
        } else {
            Snackbar.make(coordinator, "No Internet Connection!!!", Snackbar.LENGTH_SHORT).show()
        }
    }

    @Subscribe
    fun LoginResultEvent(event: Events.LoginEvent?) {
        if (event != null) {
            val token: String
            val webSocketUrl: String
            val role: String
            val name: String
            val username: String
            val contact: String
            val company: String
            val address: String
            val image: String
            try {
                val jsonObject = event.loginResponse
                if (jsonObject.getBoolean("status")) {
                    val jsonData = jsonObject.getJSONObject("data")
                    token = jsonData.getString("token")
                    webSocketUrl = jsonData.getString("web_socket_url")
                    role = jsonData.getString("role")
                    val userInfoObject = jsonData.getJSONObject("userInfo")
                    name = userInfoObject.getString("name")
                    username = userInfoObject.getString("username")
                    contact = userInfoObject.getString("contact")
                    company = userInfoObject.getString("company")
                    address = userInfoObject.getString("address")
                    image = userInfoObject.getString("image")

                    val userIdaccessToken = UserAccessToken()

                    Log.i("accessToken", token)
                    try {
                        userIdaccessToken.token = token
                        userIdaccessToken.web_socket_url = webSocketUrl
                        userIdaccessToken.role = role
                        userIdaccessToken.name = name
                        userIdaccessToken.username = username
                        userIdaccessToken.contact = contact
                        userIdaccessToken.company = company
                        userIdaccessToken.address = address
                        userIdaccessToken.image = image
                        userIdaccessToken.save()
                        Log.i("saved", "true")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    PrefUtils.setUserLoggedIn(this, true)
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else if (!jsonObject.getBoolean("status")) {
                    Toast.makeText(this@LoginActivity, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }


        }
    }
}
