package com.trackomotor.portal.trackomotor.model

/**
 * Created by bishal21 on 2/25/2018.
 */

class GeofenceModel(var id: Int?, var name: String?, var slug: String?, var description: String?, var area: String?, var type: String?, var created: String?, var geofenceDeviceList: List<GeofenceDevice>?)
