package com.trackomotor.portal.trackomotor.controller.request

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.controller.RetrofitAdapter
import com.trackomotor.portal.trackomotor.util.AppLog
import com.trackomotor.portal.trackomotor.util.ErrorUtils
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AlertTask {
    fun alertRequest(apiKey: String, accessToken: String) {
        val retrofit = RetrofitAdapter().adapter
        val alertApi = retrofit.create(AlertInterface::class.java)
        alertApi.getAlertRequest(apiKey, accessToken).enqueue(object : Callback<JsonElement> {

            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    AppLog.i(this, "Alert response: " + response.body().toString())
                    Log.i("response", response.body().toString())
                    val jsonElement = response.body().toString()
                    try {
                        val jsonObject = JSONObject(jsonElement)
                        EventBus.post(Events.VehicleAlert(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (response.code() == 401) {
                    val error = ErrorUtils.parseError(response)
                    val gson = Gson()
                    val json = gson.toJson(error)
                    Log.d("error message", json)
                    try {
                        val jsonObject = JSONObject(json)
                        EventBus.post(Events.VehicleAlert(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                AppLog.i(this, "Failure: " + t.localizedMessage)
            }


        })

    }

    fun alertRequestNext(apiKey: String, accessToken: String, url: String) {
        val retrofit = RetrofitAdapter().adapter
        val alertApi = retrofit.create(AlertInterface::class.java)
        alertApi.getNextAlertRequest(apiKey, accessToken, url).enqueue(object : Callback<JsonElement> {

            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    AppLog.i(this, "Next Alert response: " + response.body().toString())
                    Log.i("response", response.body().toString())
                    val jsonElement = response.body().toString()
                    try {
                        val jsonObject = JSONObject(jsonElement)
                        EventBus.post(Events.NextVehicleAlert(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (response.code() == 403) {
                    val error = ErrorUtils.parseError(response)
                    val gson = Gson()
                    val json = gson.toJson(error)
                    Log.d("error message", json)
                    try {
                        val jsonObject = JSONObject(json)
                        EventBus.post(Events.NextVehicleAlert(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                AppLog.i(this, "Failure: " + t.localizedMessage)
            }


        })

    }


}
