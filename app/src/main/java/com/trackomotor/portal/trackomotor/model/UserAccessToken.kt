package com.trackomotor.portal.trackomotor.model


import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.query.Delete
import com.activeandroid.query.Select

/**
 * Created by Bishal on 3/16/17.
 */
class UserAccessToken : Model {
    @Column(name = "token")
    var token: String = ""
    @Column(name = "web_socket_url")
    var web_socket_url: String = ""
    @Column(name = "role")
    var role: String = ""
    @Column(name = "name")
    var name: String = ""
    @Column(name = "username")
    var username: String = ""
    @Column(name = "contact")
    var contact: String = ""
    @Column(name = "company")
    var company: String = ""
    @Column(name = "address")
    var address: String = ""
    @Column(name = "image")
    var image: String = ""

    constructor() : super() {}

    constructor(token: String, web_socket_url: String, role: String, name: String, username: String, contact: String, company: String, address: String, image: String) : super() {
        this.token = token
        this.web_socket_url = web_socket_url
        this.role = role
        this.name = name
        this.username = username
        this.contact = contact
        this.company = company
        this.address = address
        this.image = image
    }

    companion object {

        val userToken: UserAccessToken
            get() = Select().from(UserAccessToken::class.java).executeSingle()

        fun deleteLoginData() {
            Delete().from(UserAccessToken::class.java).execute<Model>()
        }
    }
}
