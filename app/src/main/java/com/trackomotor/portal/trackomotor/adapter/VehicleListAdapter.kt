package com.trackomotor.portal.trackomotor.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.model.DepartmentsVechicle
import kotlinx.android.synthetic.main.adapter_vehicle_list.view.*


/**
 * Created by yarsha-and01 on 5/22/17.
 */

class VehicleListAdapter(private val context: Context, internal var list: List<DepartmentsVechicle>) : RecyclerView.Adapter<VehicleListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_vehicle_list, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemName.text = list[position].deviceName
        Picasso.with(context).load(list[position].vehicleTypeImage).into(holder.itemImage)

        if (list[position].deviceStatus.equals("online")) {
            holder.itemStatus.setBackgroundResource(R.drawable.online_status)
        } else {
            holder.itemStatus.setBackgroundResource(R.drawable.offline_status)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemImage = view.itemImage
        val itemName = view.itemName
        val itemStatus = view.itemStatus
    }


}
