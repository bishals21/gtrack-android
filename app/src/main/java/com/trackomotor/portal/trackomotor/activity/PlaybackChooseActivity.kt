package com.trackomotor.portal.trackomotor.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import com.squareup.otto.Subscribe
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.R
import com.trackomotor.portal.trackomotor.controller.request.VehicleTask
import com.trackomotor.portal.trackomotor.model.DepartmentsVechicle
import com.trackomotor.portal.trackomotor.model.UserAccessToken
import com.trackomotor.portal.trackomotor.util.AppText
import com.trackomotor.portal.trackomotor.util.AppUtil
import kotlinx.android.synthetic.main.activity_playback_choose.*
import org.json.JSONException
import java.text.SimpleDateFormat
import java.util.*


class PlaybackChooseActivity : AppCompatActivity() {

    private val TAG = "Playback"
    private val TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT"

    private var dateTimeDialogFragment: SwitchDateTimeDialogFragment? = null
    private var CLICKED_BUTTON = 0

    var alertDialog: AlertDialog? = null
    private var departmentsVechicleList: MutableList<DepartmentsVechicle>? = null
    private var vehicleId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playback_choose)
        EventBus.register(this)

        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar.title = "Historical Routes"
        initDialog()

        fetchVehicles()


        // Construct SwitchDateTimePicker
        dateTimeDialogFragment = supportFragmentManager.findFragmentByTag(TAG_DATETIME_FRAGMENT) as? SwitchDateTimeDialogFragment
        if (dateTimeDialogFragment == null) {
            dateTimeDialogFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            )
        }

        // Init format
        val myDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        // Assign unmodifiable values
        dateTimeDialogFragment!!.set24HoursMode(false)
        dateTimeDialogFragment!!.setMinimumDateTime(GregorianCalendar(2015, Calendar.JANUARY, 1).time)
        dateTimeDialogFragment!!.setMaximumDateTime(GregorianCalendar(2025, Calendar.DECEMBER, 31).time)

        // Define new day and month format
        try {
            dateTimeDialogFragment!!.setSimpleDateMonthAndDayFormat(SimpleDateFormat("MMMM dd", Locale.getDefault()))
        } catch (e: SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException) {
            Log.e(TAG, e.message)
        }


        dateTimeDialogFragment!!.setOnButtonClickListener(object : SwitchDateTimeDialogFragment.OnButtonClickListener {
            override fun onPositiveButtonClick(date: Date) {
                if (CLICKED_BUTTON == 1) {
                    etFrom.setText(myDateFormat.format(date))
                } else {
                    etTo.setText(myDateFormat.format(date))
                }
            }

            override fun onNegativeButtonClick(date: Date) {

            }
        })

        etFrom.setOnClickListener({
            dateTimeDialogFragment!!.startAtCalendarView()
            dateTimeDialogFragment!!.setDefaultDateTime(Calendar.getInstance().time)
            dateTimeDialogFragment!!.show(supportFragmentManager, TAG_DATETIME_FRAGMENT)
            CLICKED_BUTTON = 1
        })

        etTo.setOnClickListener({
            dateTimeDialogFragment!!.startAtCalendarView()
            dateTimeDialogFragment!!.setDefaultDateTime(Calendar.getInstance().time)
            dateTimeDialogFragment!!.show(supportFragmentManager, TAG_DATETIME_FRAGMENT)
            CLICKED_BUTTON = 2
        })

        btnSearch.setOnClickListener({
            if (validateData()) {
                val intent = Intent(this, MapsActivity::class.java)
                intent.putExtra("vehicleId", vehicleId)
                intent.putExtra("fromDate", etFrom.text.toString())
                intent.putExtra("toDate", etTo.text.toString())
                startActivity(intent)
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun initDialog() {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.progressbar_dialog, null)
        alertDialogBuilder.setView(view)
        alertDialog = alertDialogBuilder.create()
        alertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    private fun fetchVehicles() {
        if (AppUtil.isNetworkConnected(this)) {
            alertDialog!!.show()
            Log.i("network", "connected")
            VehicleTask().getVehicleListForSpinner(AppText.API_KEY, UserAccessToken.userToken.token)
        } else {
            alertDialog!!.dismiss()
            Toast.makeText(this, "Internet is not connected", Toast.LENGTH_LONG).show()
        }
    }

    @Subscribe
    fun VehicleEvent(event: Events.VehicleEvent?) {
        alertDialog!!.dismiss()
        if (event != null) {
            try {
                val jsonObject = event.vehicleResponse
                if (jsonObject.getBoolean("status")) {
                    val data = jsonObject.getJSONObject("data")
                    val departments = data.getJSONArray("departments")
                    departmentsVechicleList = ArrayList<DepartmentsVechicle>()
                    for (i in 0 until departments.length()) {
                        val dataItem = departments.getJSONObject(i)
                        val name = dataItem.getString("name")
                        val vehicleArray = dataItem.getJSONArray("vehicles")
                        for (j in 0 until vehicleArray.length()) {
                            val item = vehicleArray.getJSONObject(j)
                            val number = item.getString("number")
                            val code = item.getString("code")
                            val deviceId = item.getInt("deviceId")
                            val deviceUniqueId = item.getString("deviceUniqueId")
                            val deviceName = item.getString("deviceName")
                            val deviceStatus = item.getString("deviceStatus")
                            val vehicleType = item.getString("vehicleType")
                            val vehicleTypeImage = item.getString("vehicleTypeImage")
                            val overSpeedLimit = item.getInt("overSpeedLimit")
                            val lastUpdate = item.getString("lastUpdate")
                            val longitude = item.getString("longitude")
                            val latitude = item.getString("latitude")
                            val address = item.getString("address")
                            val speed = item.getString("speed")
                            val ignition = item.getString("ignition")
                            val charge = item.getString("charge")
                            val battery = item.getString("battery")
                            val rssi = item.getString("rssi")
                            val course = item.getString("course")
                            val alarm = item.getString("alarm")
                            val lastPositionTime = item.getString("lastPositionTime")
                            val event = item.getString("event")
                            val lastEventTime = item.getString("lastEventTime")
                            departmentsVechicleList!!.add(DepartmentsVechicle(name, number, code, deviceId, deviceUniqueId, deviceName, deviceStatus, vehicleType, vehicleTypeImage, overSpeedLimit, lastUpdate, longitude, latitude, address, speed, ignition, charge, battery, rssi, course, alarm, lastPositionTime, event, lastEventTime))
                        }

                    }


                    val adapter = ArrayAdapter<DepartmentsVechicle>(this, android.R.layout.simple_spinner_dropdown_item, departmentsVechicleList)
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    vehicleSpinner.adapter = adapter
                    vehicleSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                        override fun onItemSelected(arg0: AdapterView<*>, arg1: View, arg2: Int, arg3: Long) {
                            val departmentsVechicle: DepartmentsVechicle = departmentsVechicleList!![arg2]
                            Log.i("Selected item : ", departmentsVechicle.deviceName)
                            vehicleId = departmentsVechicle.deviceId

                        }

                        override fun onNothingSelected(arg0: AdapterView<*>) {

                        }

                    }

                } else if (!jsonObject.getBoolean("status")) {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }


        }

    }


    private fun validateData(): Boolean {
        var isChecked = true

        val fromDate = etFrom.text.toString()
        val toDate = etTo.text.toString()

        if (fromDate == "") {
            fromTextLayout.error = "From Date is required"
            isChecked = false
        }
        if (toDate == "") {
            toTextLayout.error = "To Date is required"
            isChecked = false
        }

        return isChecked
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.unregister(this)
    }
}
