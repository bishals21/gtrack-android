package com.trackomotor.portal.trackomotor.model


class PositionRequest {
    var deviceId: Int? = null
    var from: String? = null
    var to: String? = null

    constructor() {}

    constructor(deviceId:Int, from: String, to: String) {
        this.deviceId = deviceId
        this.from = from
        this.to = to
    }
}
