package com.trackomotor.portal.trackomotor.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.trackomotor.portal.trackomotor.util.PrefUtils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (PrefUtils.isUserLoggedIn(this@SplashActivity)) {
            val i = Intent(this@SplashActivity,
                    MainActivity::class.java)
            startActivity(i)
            finish()

        } else {
            val i = Intent(this@SplashActivity,
                    LoginActivity::class.java)
            startActivity(i)
            finish()
        }
    }
}
