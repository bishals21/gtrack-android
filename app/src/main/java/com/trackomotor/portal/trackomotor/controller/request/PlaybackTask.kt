package com.trackomotor.portal.trackomotor.controller.request

import android.util.Log

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.trackomotor.portal.trackomotor.EventBus.EventBus
import com.trackomotor.portal.trackomotor.EventBus.Events
import com.trackomotor.portal.trackomotor.controller.RetrofitAdapter
import com.trackomotor.portal.trackomotor.model.PositionRequest
import com.trackomotor.portal.trackomotor.util.AppLog
import com.trackomotor.portal.trackomotor.util.ErrorUtils

import org.json.JSONException
import org.json.JSONObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by bishal21 on 2/26/2018.
 */

class PlaybackTask {
    fun getPositions(apiKey: String, accessToken: String, vehicleId: Int, fromDate: String, toDate: String) {
        val retrofit = RetrofitAdapter().adapter
        val playbackInterface = retrofit.create(PlaybackInterface::class.java)
        val url = "http://portal.trackomotor.com/api/v1/company/$vehicleId/positions?from_date=$fromDate&to_date=$toDate"
        playbackInterface.getPositions(apiKey, accessToken, /*fromDate, toDate,*/ url).enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    AppLog.i(this, "Playback response: " + response.body().toString())
                    Log.i("response", response.body().toString())
                    val jsonElement = response.body().toString()
                    try {
                        val jsonObject = JSONObject(jsonElement)
                        EventBus.post(Events.PlaybackEvent(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (response.code() == 401) {
                    val error = ErrorUtils.parseError(response)
                    val gson = Gson()
                    val json = gson.toJson(error)
                    Log.d("error message", json)
                    try {
                        val jsonObject = JSONObject(json)
                        EventBus.post(Events.PlaybackEvent(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                AppLog.i(this, "Failure: " + t.localizedMessage)
            }
        })

    }

    fun getPosition(apiKey: String, accessToken: String, deviceId: Int, fromDate: String, toDate: String) {
        val retrofit = RetrofitAdapter().adapter
        val playbackInterface = retrofit.create(PlaybackInterface::class.java)
        playbackInterface.getPosition(apiKey, accessToken, PositionRequest(deviceId,fromDate,toDate)).enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    AppLog.i(this, "Playback response: " + response.body().toString())
                    Log.i("response", response.body().toString())
                    val jsonElement = response.body().toString()
                    try {
                        val jsonObject = JSONObject(jsonElement)
                        EventBus.post(Events.PlaybackEvent(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (response.code() == 401) {
                    val error = ErrorUtils.parseError(response)
                    val gson = Gson()
                    val json = gson.toJson(error)
                    Log.d("error message", json)
                    try {
                        val jsonObject = JSONObject(json)
                        EventBus.post(Events.PlaybackEvent(jsonObject))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                AppLog.i(this, "Failure: " + t.localizedMessage)
            }
        })

    }
}
