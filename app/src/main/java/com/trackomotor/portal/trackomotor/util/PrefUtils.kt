package com.trackomotor.portal.trackomotor.util

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by bishal21 on 2/5/2018.
 */

class PrefUtils(internal var context: Context) {

    internal var pref: SharedPreferences
    internal var editor: SharedPreferences.Editor


    init {
        pref = context.getSharedPreferences(SHARED_PREF_GTRACK, Context.MODE_PRIVATE)
        editor = pref.edit()

    }


    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        editor.putBoolean(isFirstRun, isFirstTime)
        editor.commit()
    }

    companion object {

        val isFirstRun = "isFirstRun"

        val SHARED_PREF_GTRACK = "gtrack"
        val EMAIL_KEY_IS_LOGGED_IN = "is_emaillogged_in"
        //first run app ?
        fun isFirstRun(context: Context): Boolean {
            return context.getSharedPreferences(SHARED_PREF_GTRACK, Context.MODE_PRIVATE).getBoolean(isFirstRun, true)
        }


        fun isUserLoggedIn(context: Context): Boolean {

            val prefs = context.getSharedPreferences(SHARED_PREF_GTRACK, Context.MODE_PRIVATE)
            return prefs.getBoolean(EMAIL_KEY_IS_LOGGED_IN, false)
        }


        fun setUserLoggedIn(context: Context, isLogin: Boolean) {
            val prefs = context.getSharedPreferences(SHARED_PREF_GTRACK, Context.MODE_PRIVATE)
            val editor = prefs.edit()
            editor.putBoolean(EMAIL_KEY_IS_LOGGED_IN, isLogin)
            editor.commit()
        }

    }


}
