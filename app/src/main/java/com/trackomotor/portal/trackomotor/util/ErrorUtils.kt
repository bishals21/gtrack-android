package com.trackomotor.portal.trackomotor.util


import com.trackomotor.portal.trackomotor.controller.RetrofitAdapter
import com.trackomotor.portal.trackomotor.model.APIError

import java.io.IOException

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response


/**
 * Created by bishal21 on 1/17/2018.
 */

object ErrorUtils {

    fun parseError(response: Response<*>): APIError {
        val converter = RetrofitAdapter().adapter
                .responseBodyConverter<APIError>(APIError::class.java, arrayOfNulls(0))

        val error: APIError

        try {
            error = converter.convert(response.errorBody())
        } catch (e: IOException) {
            return APIError()
        }

        return error
    }
}

